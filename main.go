package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"gitlab.com/krichprollsch/sudouest/server"
	"gitlab.com/krichprollsch/sudouest/so"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGHUP)
	defer cancel()

	if err := run(ctx, os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
		return
	}
}

func usage() {
	fmt.Fprint(os.Stderr, `
usage:
	sudouest [--date=2022-03-15] download
		use env var:
			SO_LOGIN and SO_PASSWORD for sudouest auth
			SO_ARCHIVES for archives location dir
	sudouest serve
		use env var:
			SO_ADDR for http server listen address
			SO_ARCHIVES for archives location dir
`)
}

func run(ctx context.Context, args []string) error {
	flagset := flag.NewFlagSet("sudouest", flag.ExitOnError)
	flagset.Usage = usage

	date := so.Date(time.Now())
	flagset.Var(&date, "date", "edition date to download")

	if err := flagset.Parse(args); err != nil {
		return fmt.Errorf("flag parse: %w", err)
	}

	args = flagset.Args()
	if len(args) != 1 {
		usage()
		return errors.New("invalid args number")
	}

	switch args[0] {
	case "download":
		ref := []string{"12B", "BPB", "PYR"}

		return download(ctx, date, ref)
	case "serve":
		addr := os.Getenv("SO_ADDR")
		if addr == "" {
			addr = "0.0.0.0:3000"
		}

		archives := os.Getenv("SO_ARCHIVES")
		if archives == "" {
			archives = "archives"
		}
		return server.Serve(ctx, addr, archives)
	default:
		usage()
		return errors.New("invalid arg")
	}
}

func download(ctx context.Context, date so.Date, ref []string) error {
	login := os.Getenv("SO_LOGIN")
	if login == "" {
		return errors.New("empty login, set SO_LOGIN env var")
	}
	passwd := os.Getenv("SO_PASSWORD")
	if passwd == "" {
		return errors.New("empty password, set SO_PASSWORD env var")
	}

	archives := os.Getenv("SO_ARCHIVES")
	if archives == "" {
		archives = "archives"
	}

	filename := filepath.Join(archives, date.String()+".pdf")
	// If the filename already exists, we have nothing to do.
	_, err := os.Stat(filename)
	if err == nil {
		return nil
	}

	socli, err := so.NewClient(login, passwd)
	if err != nil {
		return fmt.Errorf("new so cli: %w", err)
	}

	if err := socli.Login(ctx); err != nil {
		return fmt.Errorf("so login: %w", err)
	}
	if err := socli.PostOAuth2(ctx); err != nil {
		return fmt.Errorf("so oauth: %w", err)
	}

	newspapers, err := socli.NewspapersList(ctx)
	if err != nil {
		return fmt.Errorf("so newspapers: %w", err)
	}

	day := newspapers.Day(date)
	if day == nil {
		return fmt.Errorf("day not found: %v", date)
	}

	edition := day.Edition(ref)
	if edition == nil {
		return fmt.Errorf("edition not found: %s", ref)
	}

	r, err := socli.DownloadEdition(ctx, date, edition)
	if err != nil {
		return fmt.Errorf("download edition: %w", err)
	}
	defer r.Close()

	f, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("open file: %w", err)
	}
	defer f.Close()

	if _, err := io.Copy(f, r); err != nil {
		f.Close()
		// remove the file.
		os.Remove(filename)

		return fmt.Errorf("copy: %w", err)
	}

	return nil
}
