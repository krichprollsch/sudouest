FROM golang:1.21.0-alpine3.18 as builder

# Install certificates
RUN apk update && apk add --no-cache git openssh ca-certificates && update-ca-certificates

WORKDIR /src
COPY . .

ENV GOOS=linux
ENV GOARCH=amd64

RUN CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /src/sudouest /sudouest

VOLUME /archives

EXPOSE 3000

ENTRYPOINT [ "/sudouest", "serve" ]

