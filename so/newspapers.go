package so

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type Edition struct {
	Name        string    `json:"edition_name"`
	MId         string    `json:"mid"`
	Thumb       string    `json:"frontpage_url"`
	Date        time.Time `json:"generation_date"`
	EditionId   string    `json:"edition_id"`
	NewspaperId string    `json:"newspaper_id"`
}

type Day struct {
	Date     string              `json:"date"`
	Editions map[string]*Edition `json:"editions"`
}

func (d *Day) Edition(ref []string) *Edition {
	for k, edition := range d.Editions {
		for _, rref := range ref {
			if k == rref {
				return edition
			}
		}
	}

	return nil
}

type Newspapers map[string]*Day

func (n Newspapers) Day(date Date) *Day {
	d := date.String()
	for k, day := range n {
		if k == d {
			return day
		}
	}

	return nil
}

func (c *Client) NewspapersList(ctx context.Context) (Newspapers, error) {
	req, err := http.NewRequestWithContext(ctx, "GET",
		"https://kiosque.sudouest.fr/api/newspapers/list/?type=journal",
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("new req: %w", err)
	}
	req.Header.Set("Accept", "application/json")

	resp, err := c.cli.Do(req)
	if err != nil {
		return nil, fmt.Errorf("do req: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("bad status code: %d", resp.StatusCode)
	}

	var np Newspapers
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&np); err != nil {
		return nil, fmt.Errorf("resp dec: %w", err)
	}

	return np, nil
}
