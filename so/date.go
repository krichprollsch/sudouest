package so

import (
	"errors"
	"time"
)

const DateFormat = "2006-01-02"

type Date time.Time

func (d *Date) String() string {
	return time.Time(*d).Format(DateFormat)
}

func (d *Date) Set(s string) error {
	t, err := time.Parse(DateFormat, s)
	if err != nil {
		return errors.New("invalid date format")
	}
	*d = Date(t)
	return nil
}

func (d Date) IsSunday() bool {
	wd := time.Time(d).Weekday()
	return wd == time.Sunday
}
