package so

import (
	"context"
	"fmt"
	"io"
	"net/http"
)

func (c *Client) DownloadEdition(ctx context.Context,
	date Date, edition *Edition,
) (io.ReadCloser, error) {
	url := "https://kiosque.sudouest.fr/journal/" + date.String() + "/" + edition.NewspaperId + "/" + edition.EditionId + "/pdf/"
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("new req: %w", err)
	}

	resp, err := c.cli.Do(req)
	if err != nil {
		return nil, fmt.Errorf("do req: %w", err)
	}

	if resp.StatusCode >= 400 {
		resp.Body.Close()
		return nil, fmt.Errorf("bad status code: %d", resp.StatusCode)
	}

	return resp.Body, nil
}
