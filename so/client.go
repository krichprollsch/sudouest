package so

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"regexp"
	"strings"

	"golang.org/x/net/publicsuffix"
)

const UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:127.0) Gecko/20100101 Firefox/127.0"

type Client struct {
	login    string
	password string

	cli *http.Client
}

func NewClient(login, password string) (*Client, error) {
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return nil, fmt.Errorf("new cookie jar: %w", err)
	}

	return &Client{
		login:    login,
		password: password,

		cli: &http.Client{
			Jar: jar,
		},
	}, nil
}

func (c *Client) Login(ctx context.Context) error {
	urlok, err := c.loginForm(ctx)
	if err != nil {
		return fmt.Errorf("login form: %w", err)
	}

	data := url.Values{
		"login":    {c.login},
		"password": {c.password},
		"username": {""},
		"UrlOk":    {urlok},
	}
	req, err := http.NewRequestWithContext(ctx,
		"POST", "https://profil.sudouest.fr/login/login.php",
		strings.NewReader(data.Encode()),
	)
	if err != nil {
		return fmt.Errorf("new req: %w", err)
	}

	req.Header.Set("User-Agent", UserAgent)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := c.cli.Do(req)
	if err != nil {
		return fmt.Errorf("do req: %w", err)
	}
	resp.Body.Close()

	if resp.StatusCode >= 400 {
		return fmt.Errorf("bad status code: %d", resp.StatusCode)
	}

	return nil
}

var urlokRegexp = regexp.MustCompile(`name="UrlOk"\s+value="(.+)"`)

func (c *Client) loginForm(ctx context.Context) (string, error) {
	req, err := http.NewRequestWithContext(ctx, "GET",
		"https://profil.sudouest.fr/oauth2/authorize.php?response_type=code&client_id=SO-bqwNTK1azv&redirect_uri=https://kiosque.sudouest.fr/login/check-so&interactive=true",
		nil,
	)
	if err != nil {
		return "", fmt.Errorf("new req: %w", err)
	}

	req.Header.Set("User-Agent", UserAgent)

	resp, err := c.cli.Do(req)
	if err != nil {
		return "", fmt.Errorf("do req: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return "", fmt.Errorf("bad status code: %d", resp.StatusCode)
	}

	// extract url ok with the session id
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read body: %w", err)
	}

	matches := urlokRegexp.FindSubmatch(b)
	if len(matches) != 2 {
		return "", errors.New("urlok not found")
	}

	return string(matches[1]), nil
}

func (c *Client) PostOAuth2(ctx context.Context) error {
	data := url.Values{}
	data.Set("client_id", "SO-bqwNTK1azv")
	data.Set("response_type", "code")
	data.Set("redirect_uri", "https://kiosque.sudouest.fr/login/check-so")
	data.Set("state", "")
	data.Set("scope", "")
	data.Set("app_name", "Kiosque - PROD")
	data.Set("fields", "{\"SSO\":{\"id_sso\":\"user_id\",\"pseudo\":\"pseudo\",\"civilite\":\"genre\",\"prenom\":\"first_name\",\"nom\":\"last_name\",\"mail\":\"email\",\"statut_mail_confirme\":\"email_validation\",\"insee\":\"insee_code\",\"code_postal\":\"postal_code\",\"ville\":\"city\",\"date_naiss\":\"birthday\"},\"MonAvatar\":{\"avatar\":\"avatar\"},\"AbonneGSO\":{\"subscriber_ids\":\"subscriber_ids\",\"status\":\"status\",\"type\":\"type\",\"nb_archive\":\"nb_archive\"}}")
	data.Set("origine_editeur", "SO")
	data.Set("origine_service", "kiosque")
	data.Set("trusted", "1")
	data.Set("accept", "Autoriser l'accès")

	req, err := http.NewRequestWithContext(ctx, "POST",
		"https://profil.sudouest.fr/oauth2/authorize.php",
		strings.NewReader(data.Encode()),
	)
	if err != nil {
		return fmt.Errorf("new req: %w", err)
	}
	req.Header.Set("User-Agent", UserAgent)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Referer", "https://profil.sudouest.fr/oauth2/authorize.php?response_type=code&client_id=SO-bqwNTK1azv&redirect_uri=https%3A%2F%2Fkiosque.sudouest.fr%2Flogin%2Fcheck-so&interactive=true")

	resp, err := c.cli.Do(req)
	if err != nil {
		return fmt.Errorf("do req: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return fmt.Errorf("bad status code: %d", resp.StatusCode)
	}

	return nil
}
