package server

import (
	"fmt"
	"io/fs"
	"strings"
)

func list(f fs.FS) ([]string, error) {
	var pdfs []string
	err := fs.WalkDir(f, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if strings.HasSuffix(path, ".pdf") {
			pdfs = append(pdfs, path)
		}
		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("fs walkdir: %w", err)
	}

	return pdfs, nil
}
