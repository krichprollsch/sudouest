package server

import (
	"fmt"
	"io"
)

func htmlHead(w io.Writer, title string) {
	fmt.Fprintln(w, `<!DOCTYPE html>`)
	fmt.Fprintln(w, `<html>`)
	fmt.Fprintln(w, `<head>`)
	fmt.Fprintln(w, `<meta charset="UTF-8">`)
	fmt.Fprintln(w, `<title>`+title+`</title>`)
}

func htmlBody(w io.Writer) {
	fmt.Fprintln(w, `</head>`)
	fmt.Fprintln(w, `<body>`)
}

func htmlEnd(w io.Writer) {
	fmt.Fprintln(w, `</body></html>`)
}
