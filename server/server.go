package server

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
)

func Serve(ctx context.Context, address, dir string) error {
	f := os.DirFS(dir)

	router := http.NewServeMux()
	router.Handle("/download", err(download(f)))
	router.HandleFunc("/robots.txt",
		func(w http.ResponseWriter, req *http.Request) {
			fmt.Fprintln(w, `User-agent: *`)
			fmt.Fprintln(w, `Disallow: /`)
		},
	)
	router.Handle("/", err(index(f)))

	srv := &http.Server{
		Addr:    address,
		Handler: router,
		BaseContext: func(net.Listener) context.Context {
			return ctx
		},
	}

	// shutdown server on context cancelation
	go func(ctx context.Context, srv *http.Server) {
		<-ctx.Done()
		log.Println("server shutting down")
		if err := srv.Shutdown(ctx); err != nil {
			// context cancellation error is ignored.
			if !errors.Is(err, context.Canceled) {
				log.Printf("server shutdown: %v\n", err)
			}
		}
	}(ctx, srv)

	log.Printf("http server listening %s exposing %s dir\n", address, dir)
	// ListenAndServe always returns a non-nil error.
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("server: %w", err)
	}
	log.Println("server shutdown")
	return nil
}

func index(f fs.FS) handler {
	return func(w http.ResponseWriter, req *http.Request) error {
		pdfs, err := list(f)
		if err != nil {
			return fmt.Errorf("list: %w", err)
		}

		htmlHead(w, "Les editions")
		htmlBody(w)

		if l := len(pdfs); l > 0 {
			pdf := pdfs[l-1]
			fmt.Fprint(w, `<h1>`)
			fmt.Fprintln(w, `Dernière édition`)
			fmt.Fprintf(w, `<a href="/download?f=%s">%s</a>`, pdf, pdf)
			fmt.Fprintln(w, `</h1>`)
		}

		fmt.Fprintln(w, `<h1>Anciennes éditions</h1>`)
		fmt.Fprintln(w, `<ul>`)

		for i := len(pdfs) - 2; i >= 0; i-- {
			pdf := pdfs[i]

			fmt.Fprint(w, `<li>`)
			fmt.Fprintf(w, `<a href="/download?f=%s">%s</a>`, pdf, pdf)
			fmt.Fprintln(w, `</li>`)
		}

		fmt.Fprintln(w, `</ul>`)
		htmlEnd(w)

		return nil
	}
}

func download(f fs.FS) handler {
	return func(w http.ResponseWriter, req *http.Request) error {
		qf := req.URL.Query().Get("f")

		stat, err := fs.Stat(f, qf)
		if err != nil {
			http.Error(w, "bad request", 400)
			return fmt.Errorf("file stat: %w", err)
		}

		ff, err := f.Open(qf)
		if err != nil {
			http.Error(w, "bad request", 400)
			return fmt.Errorf("file open: %w", err)
		}

		w.Header().Set("Content-Type", "application/pdf")
		w.Header().Set("Content-Length", strconv.FormatInt(stat.Size(), 10))
		w.Header().Set("Content-Disposition", `inline; filename="`+qf+`"`)
		w.Header().Set("Expire", "0")
		w.Header().Set("Pragma", "public")

		io.Copy(w, ff)

		return nil
	}
}

type handler func(w http.ResponseWriter, req *http.Request) error

func err(h handler) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		if err := h(w, req); err != nil {
			http.Error(w, err.Error(), 500)
			log.Printf("%s %s: %v\n", req.Method, req.URL.Path, err)
		}
	}
}
